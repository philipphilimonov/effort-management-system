﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimeManageSystem.Models;

namespace TimeManageSystem.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            //return View();
            var name = "null";
            if (User.Identity.IsAuthenticated)
            {
                name = User.Identity.Name;
            }
            return View("Index", name);
        }

        public IActionResult Engineer()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Teamleader()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Manager()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
