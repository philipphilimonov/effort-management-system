﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimeManageSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace TimeManageSystem.Controllers
{
    public class EngineerController : Controller
    {
        private TimeContext db;
        public EngineerController(TimeContext context)
        {
            db = context;
        }
        public async Task<IActionResult> Indexe()
        {
             return View(await db.Engineers.ToListAsync());
        }
        public IActionResult Createe()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Createe(Engineer engineer)
        {
            db.Engineers.Add(engineer);
            await db.SaveChangesAsync();
            return RedirectToAction("Indexe");
        }

    }
}