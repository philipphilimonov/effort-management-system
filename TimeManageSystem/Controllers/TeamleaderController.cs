﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeManageSystem.Models;

namespace TimeManageSystem.Controllers
{
    public class TeamleaderController : Controller
    {
        private TimeContext db;
        public TeamleaderController(TimeContext context)
        {
            db = context;
        }
        public async Task<IActionResult> IndexETeamLead()
        {
            return View(await db.Team_leaders.ToListAsync());
        }
        public IActionResult CreateETeamLead()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateETeamLead(Teamleader team_leadeer)
        {
            db.Team_leaders.Add(team_leadeer);
            await db.SaveChangesAsync();
            return RedirectToAction("IndexETeamLead");
        }
    }
}
