﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeManageSystem.Models;

namespace TimeManageSystem.Controllers
{
    public class ManagerController : Controller
    {
        private TimeContext db;
        public ManagerController(TimeContext context)
        {
            db = context;
        }
        public async Task<IActionResult> IndexEManager()
        {
            return View(await db.Managers.ToListAsync());
        }
        public IActionResult CreateEManager()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateEManager(Manager manageer)
        {
            db.Managers.Add(manageer);
            await db.SaveChangesAsync();
            return RedirectToAction("IndexEManager");
        }
    }
}
