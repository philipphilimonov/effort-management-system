﻿using System;

namespace TimeManageSystem.Interfaces
{
    interface ITeamleader
    {
        int TmId { get; set; }
        string NameProject { get; set; }
        string OkTeam { get; set; }
        string Rejected { get; set; }
        string WorktimeT { get; set; }
        DateTime DateStart { get; set; }
        DateTime DateEnd { get; set; }
        string Accepted { get; set; }
        string ProjectDescrition { get; set; }         
          

    }
}
