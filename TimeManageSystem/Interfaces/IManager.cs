﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimeManageSystem.Interfaces
{
    interface IManager
    {
        int ManagId { get; set; }
        string OkManag { get; set; }
        string WorktimeM { get; set; }
        DateTime DateStart { get; set; }
        DateTime DateEnd { get; set; }
        string Accepted { get; set; }
        string ProjectDescrition { get; set; }
        string Rejected { get; set; }

    }
}
