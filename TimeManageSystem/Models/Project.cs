﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimeManageSystem.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; } // название пректа
        public string Description { get; set; } // описание
        public string Srok { get; set; } // срок работы
    }
}
