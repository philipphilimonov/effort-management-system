﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TimeManageSystem.Models
{
    public class Teamleader
    {
        [Key]
        public int TmId { get; set; }
        public string NameProject { get; set; }
        public string OkTeam { get; set; }
        public string Rejected { get; set; }
        [DataType(DataType.Time)]
        [Display(Name = " WorktimeT")]
        public string WorktimeT { get; set; }
    }
}
