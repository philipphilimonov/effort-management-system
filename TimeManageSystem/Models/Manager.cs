﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace TimeManageSystem.Models
{
    public class Manager
    {
        [Key]
        public int ManagId { get; set; }
        public string OkManag { get; set; }
        [DataType(DataType.Time)]
        [Display(Name = " WorktimeM")]
        public DateTime WorktimeM { get; set; }

    }
}
