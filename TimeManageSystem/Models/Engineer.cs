﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TimeManageSystem.Models
{
    public class Engineer
    {
        [Key]
        public int EnegerId { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = " DataTask")]
        public DateTime DataTaskStart { get; set; } //дата

        [DataType(DataType.Date)]
        [Display(Name = " DataTaskFinish")]
        public DateTime DataTaskFinish { get; set; } //дата

        public string Descriptionofwork { get; set; }//описание работы
        public string Kindofactiviti { get; set; } //вид деятельности

        [DataType(DataType.Time)]
        [Display(Name = " Worktime")]
        public DateTime Worktime { get; set; }//Время работы

        public string NameProject { get; set; }
    }
}
