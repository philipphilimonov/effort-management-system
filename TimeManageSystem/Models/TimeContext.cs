﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TimeManageSystem.Models
{
    public class TimeContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<Teamleader> Team_leaders { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public TimeContext(DbContextOptions<TimeContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
